const { Books } = require('../models');

class BookController {
    static getBook(req, res) {
        Books.findAll()
            .then(result => {
                res.render('book.ejs', { books: result})
            })
            .catch(err => {
                console.log(err);
            })
    }

    static addFormBook(req, res) {
        res.render('addBook.ejs');
    }

    static addBook (req, res) {
        const { title, status, author, pages, genre } = req.body;
        Books.create ({
            title,
            status,
            author,
            pages,
            genre
        })
            .then(result => {
                res.redirect('/books')
            })
            .catch(err => {
                res.send(err)
            })
    }

    static findById (req, res) {
        const id = req.params.id;
        Books.findOne({
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }

    static deleteBook (req, res) {
        const id = req.params.id;
        Books.destroy({
            where: { id }
        })
            .then(() => {
                res.redirect('/books')
            })
            .catch(err => {
                res.send(err)
            })
    }

    static updateFormBook(req, res) {
        res.render('editBook.ejs');
    }

    static updateBook (req, res) {
        const id = req.params.id;
        const { name, major, score } = req.body;

        Books.update({
            title,
            status,
            author,
            pages,
            genre
        }, {
            where: { id }
        })
            .then(result => {
                res.send(result)
            })
            .catch(err => {
                res.send(err)
            })
    }

}


module.exports = BookController;