const { Router } = require('express');
const router = Router();
const BookController = require('../controllers/Book');

router.get('/', BookController.getBook)
router.get('/add', BookController.addFormBook)
router.post('/add', BookController.addBook)
router.get('/delete/:id', BookController.deleteBook)
router.get('/edit/:id', BookController.updateBook)
router.post('/edit/:id', BookController.updateFormBook)
router.get('/:id', BookController.findById)


module.exports = router;
